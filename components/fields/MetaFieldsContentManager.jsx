import React from 'react'
import SettingField from './SettingField';
import {FormLayout} from '@shopify/polaris';

const MetaFieldsContentManager = ({metas, data, onChange, values}) => {

  // React.useEffect(() => {
  //   console.log(data, metas)
  // }, [data, metas, values]);

  return (
    Object.entries(metas).map(field =>
      <div style={{marginTop: '1.6rem'}}>
        <SettingField
            onChange={value => onChange(field[0], value)}
            schema={metas}
            value={values ? values[field[0]] : '' }
            field={{
              name: field[0]
            }}
          />
      </div>
    )
  )
}

export default MetaFieldsContentManager

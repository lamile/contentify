import React, {useState} from 'react'
import {TextField, ColorPicker, DropZone} from '@shopify/polaris'

const SettingField = ({field, schema, onChange, metas, value}) => {

  const [color, setColor] = useState({
    hue: 120,
    brightness: 1,
    saturation: 1
  });

  const [component, setComponent] = React.useState(null);

  React.useEffect(() => {
    if(schema && field.name in schema) {
      const {type, label, helptext} = schema[field.name]
      switch (type) {
        case 'string':
          setComponent(
            <TextField
              helpText={helptext}
              label={label}
              value={value}
              onChange={onChange}
              autoComplete="off"
            />
          )
          break;
        case 'image':
          setComponent(
            <DropZone label={label}>
              <DropZone.FileUpload />
            </DropZone>
          )
          break;
        case 'color':
          setComponent(<ColorPicker onChange={onChange}  color={color} fullWidth allowAlpha />)
          break
        default:
          break;
      }
    }
  }, [schema, field]);

  return component
}

export default SettingField

import gql from "graphql-tag";

export const QUERY_SHOP = gql`
  query {
    shop {
      id
      name
      primaryDomain {
        host
      }
      myshopifyDomain
      plan {
        displayName
      }
      timezoneAbbreviation
    }
  }
`;

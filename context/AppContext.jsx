import axios from 'axios';
import {createContext, useEffect, useState} from 'react';

const AppContext = createContext();

const AppProvider = ({shop, children}) => {

  const [data, setData] = useState({
    contentifyApi: process.env.NODE_ENV === 'development' ? 'f77e01fdde0bec15d87aaa637e80c769b83b67f0b665a2337223a1471faa19eb2782e40493f681a9a0d2deb3c391c6e5c874aefa754d382c218e3a519e865c82f8631fab92ddf88b6629ebdeeaed68c39895d573c4e35824d82c27ffa16ae718a8239ed7dbbc239fa26bfd3f21dd7bebbd4109f127987328c01cd84307536130': '',
    storeToken: null,
    contentTypes: null,
    shop
  });

  useEffect(() => {
    if(shop !== null) {
      axios({
        method: 'GET',
        url: `http://localhost:1337/api/stores?storeDomain=${shop.myshopifyDomain}`,
        headers: {"Authorization" : `Bearer ${data.contentifyApi}`}
      }).then(res => {
        if(res.data.store) {
          setData({
            ...data,
            storeToken: res.data.store.storetoken
          })
        } else {
          console.log(res.data.code)
          if(res.data.code === 404) {
            setData({
              ...data,
              accessDenied: true, accessDeniedReason: 'store_not_found'
            })
          } else {
            setData({
              ...data,
              accessDenied: true, accessDeniedReason: 'subscription_plan_missing'
            })
          }

        }
      })
    }
  }, [shop]);

  useEffect(() => {
    if(data.storeToken) {
      axios({
        method: 'GET',
        url: `http://localhost:1337/api/content-types?storeToken=${data.storeToken}&populate=*`,
        headers: {"Authorization": `Bearer ${data.contentifyApi}`}
      }).then(res => {
        if (res.data.code) {
          setData({
            ...data,
            storeToken: null
          })
        } else {
          setData({
            ...data,
            contentTypes: res.data.data
          })
        }
      })
    }
  }, [data.storeToken]);

  return (
    <AppContext.Provider value={[data, setData]}>
      {children}
    </AppContext.Provider>
  );
}

export {AppContext, AppProvider};

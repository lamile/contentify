import axios from 'axios';
import {useRouter} from 'next/router';
import {useCallback, useContext, useEffect, useRef, useState} from 'react'
import {
  Badge,
  Page,
  SkeletonPage,
  FormLayout,
  VisuallyHidden,
  Layout,
  Card,
  Select
} from '@shopify/polaris';
import MetaFieldsContentManager from '../../../../components/fields/MetaFieldsContentManager';
import SettingField from '../../../../components/fields/SettingField';
import {AppContext} from '../../../../context/AppContext';
import {useForm, Controller} from "react-hook-form";

const index = () => {
  const [data, setData] = useState(null);
  const [selectedStateCpt, setDelectedStateCpt] = useState('draft');

  const handleSelectChange = useCallback((value) => {
    setDelectedStateCpt(value)
    setValue('ct_status', value,{ shouldValidate: true, shouldDirty: true})

  }, []);
  const [appCtx] = useContext(AppContext);

  const {control, handleSubmit, setValue, getValues, formState} = useForm({
    reValidateMode: 'onChange'
  });
  const router = useRouter()

  useEffect(() => {
    if (appCtx.storeToken) {
      axios({
        method: 'GET',
        url: `http://localhost:1337/api/contents/${router.query.id}?storeToken=${appCtx.storeToken}&fields=*`,
        headers: {"Authorization": `Bearer ${appCtx.contentifyApi}`}
      }).then(res => {
        setData(() => res.data.data)
      })
    }
    return () => setData(null);
  }, [appCtx]);

  useEffect(() => {
    if (data !== null) {
      for (const [key, value] of Object.entries(data.content)) {
        setValue(key, value)
      }
    }
  }, [data]);

  const onSubmit = data => console.log(data);
  const formRef = useRef(null);

  return (
    data ?
    <Page
      breadcrumbs={[{content: 'Products', onAction: () => router.replace('/')}]}
      title={data.content.title}
      titleMetadata={<Badge status="warning">Draft</Badge>}
      compactTitle
      primaryAction={
        {
          content: 'Save', onAction: () => {
            formRef.current.click()
          },
          disabled: !formState.isDirty
        }
      }
      secondaryActions={[
        {
          content: 'Duplicate',
          accessibilityLabel: 'Secondary action label',
          onAction: () => alert('Duplicate action')
        },
        {
          content: 'View on your store',
          onAction: () => alert('View on your store action')
        }
      ]}
    >
      <form onSubmit={handleSubmit(onSubmit)}>

        <Layout>
          <Layout.Section>
            <Card title="Content" sectioned>
              <FormLayout>
                {Object.entries(data.content).map(field =>
                  field[0] !== 'metas' &&
                  <Controller
                    name={field[0]}
                    control={control}
                    render={({field}) =>
                      <SettingField
                        value={getValues(field.name)}
                        onChange={value => {
                          console.log(getValues())
                          field.onChange(value)
                        }}
                        schema={data.schema}
                        field={field}
                      />
                    }
                  />
                )}
              </FormLayout>
            </Card>
            {data.content.metas &&
            <Card title="Meta Fields" sectioned>
              <FormLayout>
                <Controller
                  name="metas"
                  control={control}
                  render={({field}) =>
                    <MetaFieldsContentManager
                      values={getValues('metas')}
                      onChange={(subField, value) =>
                        setValue('metas', {...getValues('metas'), [subField]: value}, { shouldValidate: true, shouldDirty: true})
                      }
                      getValues={getValues}
                      metas={data.schema.metas}
                      data={data.content.metas}
                    />
                  }
                />
              </FormLayout>
            </Card>
            }
          </Layout.Section>
          <Layout.Section secondary>
            <Card title="Content status" sectioned>
              <Controller
                name="ct_status"
                control={control}
                render={({field}) =>
                  <Select
                    value={selectedStateCpt}
                    options={[
                      {label: 'Brouillon', value: 'draft'},
                      {label: 'Actif', value: 'publish'}
                    ]}
                    onChange={handleSelectChange}
                  />
                }
              />
            </Card>
            <Card>
              <Card.Section title='categories'>
                LIST
              </Card.Section>
              <Card.Section subdued >
                ADD CATEGORY INPUT
              </Card.Section>
            </Card>
          </Layout.Section>
        </Layout>

        <VisuallyHidden>
          <button ref={formRef} type="submit">submit</button>
        </VisuallyHidden>

      </form>
    </Page> : <SkeletonPage primaryAction></SkeletonPage>
  )
}

export default index

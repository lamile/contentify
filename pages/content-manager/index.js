import React from 'react'
import ContentManager from '../components/views/ContentManager';

const index = () => {
  return (
    <ContentManager />
  )
}

export default index

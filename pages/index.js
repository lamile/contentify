import React, {useContext} from 'react';
import {AppContext} from '../context/AppContext';
import App from './components/App';

const Index = () => {
  const [appCtx] = useContext(AppContext)
  return (appCtx.storeToken ? <App /> : 'loading');
};

export default Index;

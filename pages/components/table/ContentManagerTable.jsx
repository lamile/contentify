import axios from 'axios';
import {useRouter} from 'next/router';
import React, {useCallback, useState} from 'react'
import {
  Card,
  ResourceList,
  ResourceItem,
  TextStyle,
  Modal,
  Button,
  EmptyState,
  Select
} from '@shopify/polaris'

const ContentManagerTable = ({selectedType, appCtx}) => {

  // const [data, setData] = useState(null);
  const [items, setItems] = useState(null);
  const [selected, setSelected] = useState({id: selectedType.id, label: selectedType.slug});

  React.useEffect(() => {
    handleChange()
    axios({
      method: 'GET',
      url: `http://localhost:1337/api/contents?storeToken=${appCtx.storeToken}&contentTypeId=${selected.id}`,
      headers: {"Authorization": `Bearer ${appCtx.contentifyApi}`}
    }).then(res => {
      setItems(() => res.data.data.reduce((acc, item) =>{
        acc.push({
          ...item.content,
          id: item.id
        })
        return acc
      }, []))
      // setData(() => res.data)
    })
  }, [selected]);

  const router = useRouter();

  const [active, setActive] = useState(true);

  const handleChange = useCallback(() => setActive(!active), [active]);

  const activator = <Button onClick={handleChange}>Select content type</Button>;

  const handleSelectChange = useCallback((value) => {
    setSelected({id:value, label: appCtx.contentTypes.find(item => parseInt(item.id) === parseInt(value)).slug })
  }, []);

  const RenderModal = ({display}) => {
    return(
      <Modal
        title="Select a content type"
        open={active}
        onClose={handleChange}
        activator={(display !== 'manual' && appCtx.contentTypes.length > 1) ? activator : null}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: handleChange
          }
        ]}
      >
        <Modal.Section>
          <Select
            label="Content types"
            options={appCtx.contentTypes.reduce((acc, item) => {
              acc.push(
                {label: item.slug, value: item.id}
              )
              return acc
            }, [])}
            onChange={handleSelectChange}
            value={selected.id}
          />
        </Modal.Section>
      </Modal>
    )
  }

  return (
    items !== null &&
    <div className="content-manager-table">
      <Card>
        <Card>
          <ResourceList
            emptyState={items.length === 0 ? (<>
              <RenderModal display="manual"/>
              <EmptyState
                heading="No content found"
                action={{content: 'Add content'}}
                secondaryAction={appCtx.contentTypes.length > 1 && {content: 'Select content type' ,onAction: () => handleChange()}}
                image="https://cdn.shopify.com/s/files/1/2376/3301/products/emptystate-files.png"
              />
              </>
            ) : undefined}
            alternateTool={<RenderModal display="auto" />}
            resourceName={{singular: selected.label, plural: `${selected.label}s`}}
            items={items}
            selectable
            renderItem={(item) => {
              const {id, title} = item;
              const apiMarkupText = <TextStyle variation="subdued">
                {`Rest endpoint: http://localhost:1337/api/contents/${item.id}?storeToken=${appCtx.storeToken}`}
              </TextStyle>
              return (
                <ResourceItem
                  id={id}
                  accessibilityLabel={`View details for ${title}`}
                  shortcutActions={[
                    {content: 'Edit', onAction: () => router.replace(`/content-manager/edit/${item.id}`)},
                    {content: 'Delete'}
                  ]}
                  name={title}
                >
                  <h3>
                    <TextStyle variation="strong">{title}</TextStyle>
                  </h3>
                  {apiMarkupText}
                </ResourceItem>
              );
            }}
          />
        </Card>
      </Card>
    </div>
  )
}

export default ContentManagerTable

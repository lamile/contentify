import React, {useCallback, useState} from 'react'
import {Button, Icon, Modal, Form, FormLayout, TextField} from '@shopify/polaris'
import {AddMajor, LandingPageMajor} from '@shopify/polaris-icons';
import cx from 'classnames/bind'

const SidebarContentTypes = ({mode, appCtx, loadTypeManager, selectedType}) => {
  const [active, setActive] = useState(false);

  const handleChange = useCallback(() => setActive(!active), [active]);

  const activator = <div onClick={handleChange}>
           <span style={{padding: '8px 0', display: 'block'}}>
            <Button textAlign="left" fullWidth plain icon={AddMajor}>New content type</Button>
           </span>
  </div>;

  return (
    <>
      <div className="sidebar-types">
        {appCtx.contentTypes.map(contentTypes =>
          <div className={cx({
            "sidebar-types__type": true,
            "sidebar-types__type--active": selectedType && selectedType.name === contentTypes.name
          })}>
          <span className="flex --align-center" onClick={() => loadTypeManager(contentTypes)}>
              <Icon
                source={LandingPageMajor}
                color="base"/>
            {contentTypes.name}
          </span>
          </div>
        )}
        {mode === 'builder' &&
        <Modal
          title="Create a new content type"
          open={active}
          activator={activator}
          onClose={handleChange}
          primaryAction={{
            content: 'Add Instagram',
            onAction: () => alert()
          }}
          secondaryActions={[
            {
              content: 'Cancel',
              onAction: handleChange
            }
          ]}
        >
          <Modal.Section>
            <Form>
              <FormLayout>
                <TextField
                  label="Email"
                  type="email"
                  autoComplete="email"
                  helpText={<span>
                    We’ll use this email address to inform you on future changes to
                    Polaris.
                  </span>}
                />
              </FormLayout>
            </Form>
          </Modal.Section>
        </Modal>
        }

      </div>
    </>
  )
}

export default SidebarContentTypes

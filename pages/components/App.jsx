import axios from 'axios';
import {useRouter} from "next/router";
import React, {useCallback, useState} from 'react'
import {RoutePropagator} from '../../components/RoutePropagator';
import {AppContext} from '../../context/AppContext';
import {Card, EmptyState, Page, Tabs} from '@shopify/polaris'
import ContentTypeBuilder from './views/ContentTypeBuilder';
import ContentManager from './views/ContentManager';

const App = () => {
  const [appCtx, setAppCtx] = React.useContext(AppContext);
  const [selected, setSelected] = useState(0);

  function generateUUID() {
    let d = new Date().getTime();

    if (window.performance && typeof window.performance.now === "function") {
      d += performance.now();
    }

    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (
        d + Math.random() * 16
      ) % 16 | 0;
      d = Math.floor(d / 16);
      return (
        c == 'x' ? r : (
          r & 0x3 | 0x8
        )
      ).toString(16);
    });

    return uuid;
  }

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  const tabs = [
    {
      id: 'all-customers-1',
      content: 'Content manager',
      component: <ContentManager/>,
      accessibilityLabel: 'All customers',
      panelID: 'all-customers-content-1'
    },
    {
      id: 'accepts-marketing-1',
      content: 'Content type builder',
      component: <ContentTypeBuilder/>,
      panelID: 'accepts-marketing-content-1'
    }
  ];

  function initStore() {
    const token = generateUUID()
    axios({
      method: 'POST',
      url: `http://localhost:1337/api/stores`,
      headers: {"Authorization": `Bearer ${appCtx.contentifyApi}`},
      data: {
        data: {
          url: appCtx.shop.myshopifyDomain,
          storetoken: token
        }
      }
    }).then(() => setAppCtx({
      ...appCtx,
      accessDeniedReason: 'subscription_plan_missing'
    }))
  }

  function acceptCharge() {
    alert()
  }

  return (
    <>
      <Page fullWidth={true} pagination={true}>
        <Card>
          {appCtx.storeToken !== null ? <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
            {tabs[selected].component}
          </Tabs> : <>
             <EmptyState
               heading={appCtx.accessDeniedReason === 'store_not_found' ? "Store not initialized" : "Plan selector"}
               action={{
                 content: appCtx.accessDeniedReason === 'store_not_found' ? "Initialize store" : "Please charge",
                 onAction: () => appCtx.accessDeniedReason === 'store_not_found' ? initStore() : acceptCharge()
               }}
               image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
             />
           </>
          }
        </Card>
      </Page>
      </>
  )
}

export default App

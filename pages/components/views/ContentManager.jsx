import {Card} from '@shopify/polaris';
import {AppContext} from '../../../context/AppContext';
import React from 'react'
import ContentManagerTable from '../table/ContentManagerTable';

const ContentManager = () => {
  const [appCtx, setAppCtx] = React.useContext(AppContext);
  const [selectedType, setSelectedType] = React.useState(null);

  React.useEffect(() => {
    if(appCtx.contentTypes) {
      setSelectedType(() => appCtx.contentTypes[0])
    }
  }, [appCtx]);

  return (
    selectedType &&
    <>
    <Card.Section title="" >
      <div className="flex">
        {appCtx.contentTypes && <>
          <ContentManagerTable selectedType={selectedType} appCtx={appCtx} setAppCtx={setAppCtx}/>
        </>}
      </div>
    </Card.Section>
    </>
  )
}

export default ContentManager

import {Card, TextContainer} from '@shopify/polaris';
import React from 'react';
import {AppContext} from '../../../context/AppContext';
import SidebarContentTypes from '../SidebarContenTypes';

const ContentTypeBuilder = () => {
  const [appCtx] = React.useContext(AppContext);
  return (
    <Card.Section title="">
      <div className="flex">
        {appCtx.contentTypes && appCtx.contentTypes.length > 0 ?
         <SidebarContentTypes appCtx={appCtx} mode="builder" /> :
         <TextContainer>Aucun type de contenu</TextContainer>
        }
      </div>
    </Card.Section>
  )
}

export default ContentTypeBuilder

import ApolloClient from "apollo-boost";
import {useEffect, useState} from 'react';
import { ApolloProvider } from "react-apollo";
import App from "next/app";
import { AppProvider } from "@shopify/polaris";
import { Provider, useAppBridge } from "@shopify/app-bridge-react";
import { authenticatedFetch } from "@shopify/app-bridge-utils";
import { Redirect } from "@shopify/app-bridge/actions";
import "@shopify/polaris/dist/styles.css";
import translations from "@shopify/polaris/locales/en.json";
import './app.scss'
import {RoutePropagator} from '../components/RoutePropagator';
import {AppProvider as GlobalApp} from '../context/AppContext';
import {
  Provider as AppBridgeProvider,
} from "@shopify/app-bridge-react";
import {useRouteChangeLoader} from '../hooks/useRouteChangeLoader';
import {QUERY_SHOP} from '../query/shop';

function userLoggedInFetch(app) {
  const fetchFunction = authenticatedFetch(app);

  return async (uri, options) => {
    const response = await fetchFunction(uri, options);

    if (
      response.headers.get("X-Shopify-API-Request-Failure-Reauthorize") === "1"
    ) {
      const authUrlHeader = response.headers.get(
        "X-Shopify-API-Request-Failure-Reauthorize-Url"
      );

      const redirect = Redirect.create(app);
      redirect.dispatch(Redirect.Action.APP, authUrlHeader || `/auth`);
      return null;
    }

    return response;
  };
}

function MyProvider(props) {
  const app = useAppBridge();
  useRouteChangeLoader();

  const client = new ApolloClient({
    fetch: userLoggedInFetch(app),
    fetchOptions: {
      credentials: "include",
    },
  });

  const Component = props.Component;
  const [shop, setShop] = useState(null);

  useEffect(() => {
    client.query({
      query: QUERY_SHOP
    }).then(result => setShop(result.data.shop));
  }, []);

  return (
    shop &&
    <ApolloProvider client={client}>
      <GlobalApp shop={shop}>
        <Component shop={shop} {...props} key={props.apiKey} client={client} app={app} />
      </GlobalApp>
    </ApolloProvider>
  );
}

class MyApp extends App {
  render() {
    const { Component, pageProps, host } = this.props;

    return (
      <AppBridgeProvider
        config={{
          host: host,
          apiKey: API_KEY,
          forceRedirect: true,
        }}
      >
        <RoutePropagator />
      <AppProvider i18n={translations}>
        <Provider
          config={{
            apiKey: API_KEY,
            host: host,
            forceRedirect: true,
          }}
        >
          <MyProvider Component={Component} {...pageProps} />
        </Provider>
      </AppProvider>
      </AppBridgeProvider>
    );
  }
}

MyApp.getInitialProps = async ({ ctx }) => {
  return {
    host: ctx.query.host,
  };
};

export default MyApp;

import ContentManager from './components/views/Home';

export const views = [
  {
    label: "Home",
    slug: "home",
    component: <ContentManager />
  },
  {
    label: "Editor",
    slug: "editor",
    component: <ContentManager />
  }
]

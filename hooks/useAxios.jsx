import {useState } from 'react';
import axios from 'axios';

export default (url, method = 'GET') => {
  const [data, setData] = useState(null);
    (async () => {
      setData(null);
      const res = await axios({
        method: method,
        url,
        headers: {"Authorization" : AuthStr}
      })
      setData(res.data);
    })();
  return data;
};
